const {Shop, Item} = require("../src/gilded_rose");
const fs = require('fs')



const itemsMock = [
  new Item("+5 Dexterity Vest", 10, 20),
  new Item("Aged Brie", 2, 0),
  new Item("Elixir of the Mongoose", 5, 7),
  new Item("Sulfuras, Hand of Ragnaros", 0, 80),
  new Item("Sulfuras, Hand of Ragnaros", -1, 80),
  new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
  new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
  new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),

  // This Conjured item does not work properly yet
  new Item("Conjured Mana Cake", 3, 6),
];


describe("Gilded Rose", function() {
  it("should be the same output as the one stored", function() {
    const gildedRose = new Shop(itemsMock);

    let data;
    let lastValue;


    for (let day = 1; day < 32; day++) {
      gildedRose.updateQuality();

      try {
        day = day < 10 ? "0" + day : day;
        let filename = './txt/'+ day + '-' + '01-20_sortie.json';
        data = fs.readFileSync(filename, 'utf8');
        if(lastValue) {
          itemsMock.forEach(item => {
            lastValue.forEach(lastValueItem => {
              if(lastValueItem.name == item.name) {
                item.quality += lastValueItem.quality;
                item.sellIn += lastValueItem.sellIn;
              }
            })
          });
        }

        expect(JSON.parse(JSON.stringify(itemsMock))).toStrictEqual(JSON.parse(data));

        lastValue = itemsMock;
      } catch(e) {
          console.log('Error:', e.stack);
      }
    }

  });
});
