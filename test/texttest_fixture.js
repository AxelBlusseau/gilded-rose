
const { Shop, Item } = require("../src/gilded_rose");
const fs = require('fs')

const items = [
  new Item("+5 Dexterity Vest", 10, 20),
  new Item("Aged Brie", 2, 0),
  new Item("Elixir of the Mongoose", 5, 7),
  new Item("Sulfuras, Hand of Ragnaros", 0, 80),
  new Item("Sulfuras, Hand of Ragnaros", -1, 80),
  new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
  new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
  new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),

  // This Conjured item does not work properly yet
  new Item("Conjured Mana Cake", 3, 6),
];

const days = Number(process.argv[2]) || 32;
const gildedRose = new Shop(items);

console.log("OMGHAI!");
let lastValue;
for (let day = 1; day < days; day++) {
  console.log(`\n-------- day ${day} --------`);
  console.log("name, sellIn, quality");
  items.forEach(item => console.log(`${item.name}, ${item.sellIn}, ${item.quality}`));
  gildedRose.updateQuality();

  if(lastValue) {
    items.forEach(item => {
      lastValue.forEach(lastValueItem => {
        if(lastValueItem.name == item.name) {
          item.quality += lastValueItem.quality;
          item.sellIn += lastValueItem.sellIn;
        }
      })
    });
  }

  console.log('items: ' + JSON.stringify(items));
  day = day < 10 ? "0" + day : day;
  fs.writeFileSync('../txt/'+ day + '-' + '01-20_sortie.json', JSON.stringify(items));

  lastValue = items;
}